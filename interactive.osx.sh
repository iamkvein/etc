#
# any/interactive.d/osx
#

#### Aliases ####

# Copy current directory path to clipboard
alias pcp='echo -n `pwd` | pbcopy'

# Show/hide hidden files in Finder
alias showhidden="defaults write com.apple.Finder AppleShowAllFiles -bool true && killall Finder"
alias hidehidden="defaults write com.apple.Finder AppleShowAllFiles -bool false && killall Finder"

# Empty the Trash on all mounted volumes and the main HDD
alias emptytrash="sudo rm -rfv ~/.Trash; sudo rm -rfv /Volumes/*/.Trashes"
# https://github.com/mathiasbynens/dotfiles/blob/master/.aliases
# Clean up LaunchServices to remove duplicates in the “Open With” menu
# alias lscleanup="/System/Library/Frameworks/CoreServices.framework/Frameworks/LaunchServices.framework/Support/lsregister -kill -r -domain local -domain system -domain user && killall Finder"

#### Functions ####

# Send file to trash
# function trashfile {
#   mv -f "$@" ~/.Trash
# }

# Get infos about a user
# function userinfo {
#   if [ ! $# -eq 1 ]; then
#     echo "Usage: userinfo <username>"
#   else
#     dscl . -read "/Users/$1" RealName UniqueID UserShell NFSHomeDirectory PrimaryGroupID RecordName
#   fi
# }

# List users
# function lsusers {
#   dscl . -list /Users UniqueID | awk '!/^_/ {print $2 ": " $1 }' | sort -n
# }

# List groups
# function lsgroups {
#   dscl . -list /Groups PrimaryGroupID | awk '!/^_/ {print $2 ": " $1}' | sort -n
# }

# Brew completions
for file in $(brew --prefix)/etc/bash_completion.d/*; do
  [ -r "$file" ] && . "$file"
done
unset file
type _git &>/dev/null && complete -F _git g

[ -r ~/.etc/osx.aliases.sh ] \
  && . ~/.etc/osx.aliases.sh
