#!/usr/bin/env bash
#
# Osx setup script
#

echo

########## Général ##########

echo "  Général"

echo "    Étend la boîte de dialogue de sauvegarde par défaut"
defaults write -g NSNavPanelExpandedStateForSaveMode -bool YES

echo "    Étend la boîte de dialogue d'impression par défaut"
defaults write NSGlobalDomain PMPrintingExpandedStateForPrint -bool true

echo "    Cache les icônes Time Machine et volume"
defaults write com.apple.systemuiserver menuExtras -array "/System/Library/CoreServices/Menu Extras/Bluetooth.menu" "/System/Library/CoreServices/Menu Extras/AirPort.menu" "/System/Library/CoreServices/Menu Extras/Battery.menu" "/System/Library/CoreServices/Menu Extras/Clock.menu"

echo "    Supprime les animation d'ouverture et fermeture de fenêtres"
defaults write NSGlobalDomain NSAutomaticWindowAnimationsEnabled -bool false

echo "    Augmente la vitesse de redimensionnement pour les apps Cocoa"
defaults write NSGlobalDomain NSWindowResizeTime -float 0.001

echo "    Sauvegarde sur le DD par défaut (pas iCloud)"
defaults write NSGlobalDomain NSDocumentSaveNewDocumentsToCloud -bool false

echo "    Configure la fermeture automatique de l'app d'impression après la fin du job"
defaults write com.apple.print.PrintingPrefs "Quit When Finished" -bool true

echo "    Supprimme l'alerte « Êtes-vous sûr de vouloir ouvrir l'application »"
defaults write com.apple.LaunchServices LSQuarantine -bool false

echo "    Désactive l'extinction automatique des apps inactives"
defaults write NSGlobalDomain NSDisableAutomaticTermination -bool true

# Désactive Resume dans tous le système
#defaults write NSGlobalDomain NSQuitAlwaysKeepsWindows -bool false

echo "    Active l'affichage de l'adresse IP, hostname, version de l'OS, etc. en cliquant l'horloge dans le panneau de login"
sudo defaults write /Library/Preferences/com.apple.loginwindow AdminHostInfo HostName

echo "    Utilise seulement UTF-8 dans le Terminal"
defaults write com.apple.terminal StringEncodings -array 4

########## Clavier et souris ##########

echo "  Clavier et souris"

echo "    Désactive le scroll « naturel » de Lion"
defaults write NSGlobalDomain com.apple.swipescrolldirection -bool false

echo "    Active l'accès complet du clavier pour tous contrôles (ex : tab dans les fenêtres modales)"
defaults write NSGlobalDomain AppleKeyboardUIMode -int 3

echo "    Active l'accès des technologies assistives"
echo -n 'a' | sudo tee /private/var/db/.AccessibilityAPIEnabled > /dev/null 2>&1
sudo chmod 444 /private/var/db/.AccessibilityAPIEnabled

echo "    Active ^ + mouvement pour zoomer"
defaults write com.apple.universalaccess closeViewScrollWheelToggle -bool true
defaults write com.apple.universalaccess HIDScrollZoomModifierMask -int 262144

echo "    Configure le suivit du focus du clavier en zoomant"
defaults write com.apple.universalaccess closeViewZoomFollowsFocus -bool true

echo "    Désactive l'auto-correction"
defaults write NSGlobalDomain NSAutomaticSpellingCorrectionEnabled -bool false

########## Écran ##########

echo "  Écran"

echo "    Demande le mdp en retour de veille ou commencement écran de veille"
defaults write com.apple.screensaver askForPassword -int 1
defaults write com.apple.screensaver askForPasswordDelay -int 0

echo "    Sauvegarder les captures dans un sous-dossier d'Images"
defaults write com.apple.screencapture location -string "$HOME/Pictures/Captures"

echo "    Capturer en .png (autres options : bmp, gif, jpg, pdf, tiff)"
defaults write com.apple.screencapture type -string "png"

echo "    Désactive les ombres dans les captures"
defaults write com.apple.screencapture disable-shadow -bool true

echo "    Active le rendement sub-pixel sur les LCD Apple"
defaults write NSGlobalDomain AppleFontSmoothing -int 2

# Activer mode HiDPI (nécessite redémarrage)
#sudo defaults write /Library/Preferences/com.apple.windowserver DisplayResolutionEnabled -bool true

########## Finder ##########

echo "  Finder"

echo "    Désactive les animations de fenêtres"
defaults write com.apple.finder DisableAllAnimations -bool true

echo "    Affiche les icônes DD, serveurs et périphériques retirable sur le bureau"
defaults write com.apple.finder ShowExternalHardDrivesOnDesktop -bool true
defaults write com.apple.finder ShowHardDrivesOnDesktop -bool true
defaults write com.apple.finder ShowMountedServersOnDesktop -bool true
defaults write com.apple.finder ShowRemovableMediaOnDesktop -bool true

echo "    Affiche toutes les extensions de fichiers"
defaults write NSGlobalDomain AppleShowAllExtensions -bool true

echo "    Autorise la sélection de texte dans Quicklook"
defaults write com.apple.finder QLEnableTextSelection -bool true

echo "    Affiche les chemins POSIX complets dans le titre de fenêtre"
defaults write com.apple.finder _FXShowPosixPathInTitle -bool true

echo "    En recherche, chercher dans le dossier courant par défaut"
defaults write com.apple.finder FXDefaultSearchScope -string "SCcf"

echo "    Supprime l'alerte de changement d'extension"
defaults write com.apple.finder FXEnableExtensionChangeWarning -bool false

echo "    Éviter de créer des .DS_Store sur les disques réseau"
defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool true

echo "    Vue colonne dans toutes les fenêtres Finder"
# Autres modes : `icnv`, `clmv`, `Flwv`
defaults write com.apple.finder FXPreferredViewStyle -string "clmv"

#echo "    Configure le vidage la corbeille en mode sécurisé par défaut"
#defaults write com.apple.finder EmptyTrashSecurely -bool true

echo "    Affiche ~/Bibliothèque"
chflags nohidden ~/Library

echo "    Affiche les infos de l'élement sous les icônes du bureau"
/usr/libexec/PlistBuddy -c "Set :DesktopViewSettings:IconViewSettings:showItemInfo true" ~/Library/Preferences/com.apple.finder.plist

# Afficher les fichier cachés par défaut
##defaults write com.apple.finder AppleShowAllFiles -bool true

# Activer AirDrop par Ethernet et sur les Lion le supportant
#defaults write com.apple.NetworkBrowser BrowseAllInterfaces -bool true

########## Dock ##########

echo "  Dock"

echo "    Affiche l'indicateur sur les apps ouvertes"
defaults write com.apple.dock show-process-indicators -bool true

echo "    Désactive les animations des apps en ouverture"
defaults write com.apple.dock launchanim -bool false

echo "    Supprime le délai d'auto-fermeture du Dock"
defaults write com.apple.Dock autohide-delay -float 0

#echo "    Supprime les animations d'affichage/masquage du Dock"
#defaults write com.apple.dock autohide-time-modifier -float 0

echo "    Configure le Dock en 2D"
defaults write com.apple.dock no-glass -bool true

echo "    Configure le Dock à droite"
defaults write com.apple.dock orientation -string right

echo "    Affiche/masque automatiquement le Dock"
defaults write com.apple.dock autohide -bool true

echo "    Affiche en transparence les icônes des apps masqués"
defaults write com.apple.dock showhidden -bool true

echo "    Active la notification de piste iTunes dans le Dock"
defaults write com.apple.dock itunes-notifications -bool true

# Affiche uniquement les apps ouvertes
defaults write com.apple.dock static-only -bool TRUE

########## Dashboard ##########

echo "  Dashboard"

echo "    Active le mode dev (autorise à mettre des gadgets sur le bureau)"
defaults write com.apple.dashboard devmode -bool true

######### Spotlight #########

echo "  Spotlight"

#echo "    Désactive spotlight"
#sudo launchctl unload -w /System/Library/LaunchDaemons/com.apple.metadata.mds.plist

########## Mission Control ##########

echo "  Mission Control"

echo "    Accélère les animations Mission Control"
defaults write com.apple.dock expose-animation-duration -float 0.1

echo "    Ne montre pas Dashboard comme un espace"
defaults write com.apple.dock dashboard-in-overlay -bool true

########## Launchpad ##########

echo "  Launchpad"

echo "    Réinitialisation"
find ~/Library/Application\ Support/Dock -name "*.db" -maxdepth 1 -delete

########## Safari ##########

echo "  Safari"

echo "    Désactive TopSite"
defaults write com.apple.Safari DebugSafari4IncludeTopSites -bool FALSE

echo "    Configure la page d'accueil sur about:blank pour une ouverture plus rapide"
defaults write com.apple.Safari HomePage -string "about:blank"

echo "    Désactive l'ouverture des documents « sûrs »"
defaults write com.apple.Safari AutoOpenSafeDownloads -bool false

echo "    Cache la barre des signets"
defaults write com.apple.Safari ShowFavoritesBar -bool false

echo "    Désactive le cache des aperçus de l'historique et de TopSite"
defaults write com.apple.Safari DebugSnapshotsUpdatePolicy -int 2

echo "    Active le menu debug"
defaults write com.apple.Safari IncludeInternalDebugMenu -bool true

echo "    Active le menu développeur et l'inspecteur web"
defaults write com.apple.Safari IncludeDevelopMenu -bool true
defaults write com.apple.Safari WebKitDeveloperExtrasEnabledPreferenceKey -bool true
defaults write com.apple.Safari com.apple.Safari.ContentPageGroupIdentifier.WebKit2DeveloperExtrasEnabled -bool true

echo "    Ajoute un élément dans le menu contextuel pour afficher l'inspecteur web"
defaults write NSGlobalDomain WebKitDeveloperExtras -bool true

echo "    Supprime les icônes inutiles dans la barre des signets"
defaults write com.apple.Safari ProxiesInBookmarksBar "()"

########## Mail ##########

echo "  Mail"

echo "    Désactive les animations envoyer/répondre"
defaults write com.apple.mail DisableReplyAnimations -bool true
defaults write com.apple.mail DisableSendAnimations -bool true

echo "    Configure copier les adresses mail comme `foo@bar.com` au lieu de `Foo Bar <foo@bar.com>`"
defaults write com.apple.mail AddressesIncludeNameOnPasteboard -bool false

echo "    Configure le raccourcis ⌘ + Entrer pour envoyer le message"
defaults write com.apple.mail NSUserKeyEquivalents -dict-add "Send" "@\\U21a9"

########## iTunes ##########

echo "  iTunes"

echo "    Supprime les icônes d'achat"
defaults write com.apple.iTunes show-store-link-arrows -bool false

echo "    Supprime la barre latérale Genius"
defaults write com.apple.iTunes disableGeniusSidebar -bool true

echo "    Supprime tous ce qui concerne Ping"
defaults write com.apple.iTunes disablePingSidebar -bool true
defaults write com.apple.iTunes disablePing -bool true

echo "    Configure ⌘ + F pour donner le focus à la barre de recherche"
defaults write com.apple.iTunes NSUserKeyEquivalents -dict-add "Target Search Field" "@F"

########## TextEdit ##########

echo "  TextEdit"

echo "    Configure le texte plein par défaut pour les nouveaux documents"
defaults write com.apple.TextEdit RichText -int 0

echo "    Configure Ouvrir/Sauvegarder en UTF-8"
defaults write com.apple.TextEdit PlainTextEncoding -int 4
defaults write com.apple.TextEdit PlainTextEncodingForWrite -int 4

########## Fin ##########

for app in "Dashboard" "Dock" "Finder" "Mail" "Safari" "Terminal" "iTunes"; do
killall "$app" &> /dev/null
done

echo
echo "Terminé.
Certains changement peuvent nécessiter un redémarrage / extinction de session pour prendre effet."
echo
