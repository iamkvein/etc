
#### Common ####

alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias vi='vim'
alias vi.='vim .'
alias grep='grep --color=auto'
alias less='less -F'
alias h='history'
alias j='jobs -l'
# alias al='alias | grep'

#### Browsing ####

alias ..='cd ../'
alias ...='cd ../../'
alias ....="cd ../../../"
alias .....="cd ../../../../"
alias ~="cd ~"
alias -- -="cd -"

#### Listing ####

alias la='ls -A'
alias ll='ls -lh'
alias lla='ls -Alh'
# alias cla='clear;pwd;ls -A'
# alias cll='clear;pwd;ls -lh'
# alias clla='clear;pwd;ls -Alh'

# List files ordered
# alias llmodified='ls -lt'
# alias llaccessed='ls -lu'
# alias llcreated='ls -lU'
# alias llsize='ls -lS'

# List directories with size
alias ldir='du -sh *'

# Show hidden files
alias l.='ls -d .*'

# List directories sorted by size desc
alias ldirsorted='du -sh * | sort -rn'

# List 10 biggest directories with sizes
alias bigdirs='du -sh * | sort -rn | head -10'

if is_program_installed tree; then
  # Colored tree
  alias tree='tree -C'
  # Tree only directories
  alias treed='tree -C -d'
fi

#### Files and dirs ####

# Remove all empty dirs in ./
# alias rmemptydirs='find ./ -maxdepth 1 -empty -type d -delete'
# Get the size of current directory in human readable format
alias dirsize='du -h | awk '\''{print $1}'\'' | tail -1'

#### Volumes ####

alias ls-mounted='mount | column -t | awk '\''{print $3}'\'' | grep --color=never /Volumes'
alias ll-mounted='mount | column -t'

#### Cleaning, Performances ####

# Speed up terminal launch
alias speedup='sudo rm /private/var/log/asl/*.asl'
# Clear Apple mail cache
# alias cmailcache='rm -f ~/Library/Caches/com.apple.mail/Cache.db'

#### Stats ####

# See what’s gobbling the memory
# alias watgobblemem='ps -o time,ppid,pid,nice,pcpu,pmem,user,comm -A | sort -n -k 6 | tail -15'

#### Git ####

alias grm='git rm'
alias grmd='git ls-files --deleted | xargs git rm'
alias ga='git add'
alias gci='git commit'
alias gcm='git commit -m'
alias gcamend='git commit --amend'
alias gcl='git clone'
alias gclr='git clone --recursive'
alias gph='git push'
alias gpl='git pull'
alias gs='git status'
alias gunstage='git reset HEAD --'
alias glg='git log'
alias glog='git log --graph --pretty=format:"%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset" --abbrev-commit'
alias glast='git log -1 HEAD'
alias gbr='git branch'
alias gco='git checkout'
alias gsb='git submodule'
alias gsbi='git submodule init'
alias gsbu='git submodule update'
alias gsba='git submodule add'
alias gsbpl='git submodule foreach git pull origin master'

#### Network ####

# alias get-public-ip='curl ifconfig.me'
# alias p3='ping -c 3'

#### Utilities ###

alias gensalt='openssl rand -base64 32'
alias rand64='openssl rand -base64'
alias randhex='openssl rand -hex'
alias pyserve='python -m SimpleHTTPServer'

#### TMUX ####

alias sess='tmux new-session -s main -d && tmux rename-window -t main:0 scratch && tmux attach-session -t main'

