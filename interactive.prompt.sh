#
# any/interactive.d/prompt
#

# function __git_prompt {
#   local git_status=`git status -unormal 2>&1`
#
#   if [[ "$git_status" =~ 'Not a git repository' ]]; then
#     pr=""
#   else
#     pr=""
#
#     if [[ "$git_status" =~ 'Untracked files' ]]; then
#       [[ $pr = "" ]] && pr=$pr"Untracked" || pr=$pr", untracked"
#     fi
#
#     if [[ "$git_status" =~ 'Changed but not updated' ]]; then
#       [[ $pr = "" ]] && pr=$pr"Changed" || pr=$pr", changed"
#     fi
#
#     if [[ "$git_status" =~ 'Changes not staged for commit' ]]; then
#       [[ $pr = "" ]] && pr=$pr"Not staged" || pr=$pr", not staged"
#     fi
#
#     if [[ "$git_status" =~ 'Changes to be committed' ]]; then
#       [[ $pr = "" ]] && pr=$pr"Staged" || pr=$pr", staged"
#     fi
#
#     if [[ $pr = "" ]]; then
#       pr=$pr"Clean"
#     fi
#
#     pr=$pr
#   fi
#
#   echo "$pr"
# }

function __dir_prompt {
  local current_dir="$@"
  local user_color="$FG_GREEN"

  # Number of files in current dir
  # local numfiles=$(ls -a | wc -w | xargs echo)

  # if [[ $OSTYPE =~ "darwin" ]]; then
  #   dirsize=$(stat -f "%z" "$current_dir")
  #
  #     # If dir is a symlink then get target
  #   if [[ -h "$current_dir" ]]; then
  #       local link_stats=$(stat -f '%Y' "$current_dir")
  #       slink="-> $link_stats"
  #   fi
  # elif [[ $OSTYPE =~ "linux" ]]; then
  #   dirsize=$(stat --format "%s" $1)
  # fi

  [[ "$OSTYPE" =~ "darwin" ]] && user_color="$FG_PURPLE"
  [[ "$USER" = "root" ]] && user_color="$FG_RED"

  echo "${user_color}\u${COLOR_RESET}@\h:\W${slink:-}"
}

function prompt_cmd {
  # Status of last command -> success and ~> fail
  [[ $? -eq 0 ]] && laststatus="${FG_GREEN}->" || laststatus="${FG_RED}~>"

  # Directory infos
  local dir_infos=$(__dir_prompt $PWD)

  # Set window title to dir path
  echo -n $'\e'"]0;$PWD"$'\a'

  export PS1="\n[${dir_infos}] ${laststatus}${COLOR_RESET} "
}

declare -r PROMPT_COMMAND='prompt_cmd' &>/dev/null

