
# Check win sizes after each command and change lines and columns is necessary
shopt -s checkwinsize
# Correct minor spelling errors when cd
shopt -s cdspell
# Case-insensitive globbing in pathname expansion
shopt -s nocaseglob
# Enable globbing dotfiles
shopt -s dotglob

# Kill the bell
set bell-style off
# Notify of bg job completion immediately
set -o notify

# No new mail notification
unset MAILCHECK

# French prefered, use UTF-8
# `locale -a` list all supported locales
export LC_ALL="fr_FR.UTF-8"
export LANG="fr_FR"

# History settings
export HISTCONTROL=ignoredups
export HISTSIZE=5000
export HISTFILESIZE=1000
export HISTIGNORE="&:ls:ll:la:l.:pwd:exit:clear"
# Append in history rather than overwrite
shopt -s histappend
# When !! load found command for verification instead runing it dirrectly
shopt -s histverify
# Keep multiline commands in one line in history
shopt -s cmdhist

# Color settings
export CLICOLOR=true
export LSCOLORS=ExFxBxDxCxegedabagacad

# Default programs
export EDITOR=vim
export GIT_EDITOR=vim
export PAGER=less

# Lynx config file
export LYNX_CFG=~/.etc/lynx.config

# Define $PATH
PATH=''
path_unshift /usr/X11/bin
path_unshift /sbin
path_unshift /usr/sbin
path_unshift /bin
path_unshift /usr/bin
path_unshift /usr/local/sbin
path_unshift /usr/local/bin
path_unshift /opt/bin
path_unshift /opt/local/bin
path_unshift /usr/local/opt/php55
path_unshift /usr/local/opt/ruby/bin
path_unshift /usr/local/share/npm/bin
path_unshift ~/.bin
path_unshift ./vendor/bin

# Define $CDPATH
CDPATH=''
path_unshift /usr/local CDPATH
path_unshift /usr CDPATH
path_unshift /etc CDPATH
path_unshift ~/.etc CDPATH
# Add relevant /Volumes/Data directories if exists
if [ -d /Volumes/Data ]; then
  export DATA_DIR=/Volumes/Data
  path_unshift ${DATA_DIR}/Code CDPATH
fi
path_unshift ~ CDPATH
path_unshift . CDPATH

# Define NODE_PATH
NODE_PATH=''
path_unshift /usr/local/share/npm/lib/node_modules/ NODE_PATH
path_unshift /usr/local/lib/node_modules/ NODE_PATH

# Cargo
if [ -d "$HOME/.cargo" ]; then
  export PATH="$HOME/.cargo/bin:$PATH"
fi

# https://github.com/mathiasbynens/dotfiles/blob/master/.bash_profile
# Enable some Bash 4 features when possible:
#  * `autocd`, e.g. `**/qux` will enter `./foo/bar/baz/qux`
#  * Recursive globbing, e.g. `echo **/*.txt`
for option in autocd globstar; do
  shopt -s "$option" 2> /dev/null
done

