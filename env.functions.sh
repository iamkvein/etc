
# Check if a program is installed
# Usage: is_program_installed cd
function is_program_installed {
  local return_=0
  type $1 &>/dev/null || { return_=1; }
  return $return_
}

# Remove a path in a path list
# Usage: path_remove /some/dir $SOME_PATH
# Second argument defaults to $PATH
function path_remove {
  local IFS=':'
  local path_var=${2:-PATH}
  local newpath

  for dir in ${!path_var}; do
    IFS=''
    if [ "$dir" != "$1" ]; then
      newpath=$newpath:$dir
    fi
  done

  export $path_var="${newpath#:}"
}

# Add a path at the end of a path list
# Usage: path_push /some/dir $SOME_PATH
# Second argument defaults to $PATH
function path_push {
  local path_var=${2:-PATH}
  local path_value=${!path_var}
  path_remove "$1" "$path_var"
  export $path_var="${path_value:+${path_value}:}${1}"
}

# Add a path at the beginning of a path list
# Usage: path_unshift /some/dir $SOME_PATH
# Second argument defaults to $PATH
function path_unshift {
  local path_var=${2:-PATH}
  local path_value=${!path_var}
  path_remove "$1" "$path_var"
  export $path_var="${1}${path_value:+:${path_value}}"
}

