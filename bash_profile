
[ -r ~/.etc/env.functions.sh ] \
  && . ~/.etc/env.functions.sh

[ -r ~/.etc/login.sh ] \
  && . ~/.etc/login.sh

[ "$PS1" ] \
  && . ~/.etc/interactive.sh

