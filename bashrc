
[ -r ~/.etc/env.functions.sh ] \
  && . ~/.etc/env.functions.sh

[ -r ~/.etc/interactive.sh ] \
  && . ~/.etc/interactive.sh

[ -r ~/.etc/private/bashrc ] \
  && . ~/.etc/private/bashrc

[ -r ~/.localrc ] \
  && . ~/.localrc

