
# Text
FG_BLACK='\[\e[0;30m\]'
FG_RED='\[\e[0;31m\]'
FG_GREEN='\[\e[0;32m\]'
FG_YELLOW='\[\e[0;33m\]'
FG_BLUE='\[\e[0;34m\]'
FG_PURPLE='\[\e[0;35m\]'
FG_CYAN='\[\e[0;36m\]'
FG_WHITE='\[\e[0;37m\]'

# Bold
BLD_BLACK='\[\e[1;30m\]'
BLD_RED='\[\e[1;31m\]'
BLD_GREEN='\[\e[1;32m\]'
BLD_YELLOW='\[\e[1;33m\]'
BLD_BLUE='\[\e[1;34m\]'
BLD_PURPLE='\[\e[1;35m\]'
BLD_CYAN='\[\e[1;36m\]'
BLD_WHITE='\[\e[1;37m\]'

# Underline
UDl_BLACK='\[\e[4;30m\]'
UDl_RED='\[\e[4;31m\]'
UDl_GREEN='\[\e[4;32m\]'
UDl_YELLOW='\[\e[4;33m\]'
UDl_BLUE='\[\e[4;34m\]'
UDl_PURPLE='\[\e[4;35m\]'
UDl_CYAN='\[\e[4;36m\]'
UDl_WHITE='\[\e[4;37m\]'

# Background
BG_BLACK='\[\e[40m\]'
BG_RED='\[\e[41m\]'
BG_GREEN='\[\e[42m\]'
BG_YELLOW='\[\e[43m\]'
BG_BLUE='\[\e[44m\]'
BG_PURPLE='\[\e[45m\]'
BG_CYAN='\[\e[46m\]'
BG_WHITE='\[\e[47m\]'

# Reset
COLOR_RESET='\[\e[0m\]'

