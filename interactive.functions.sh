#
# any/interactive.d/functions
#

#### Working with files ####

# Batch change file extension
# Usage: chext foo bar
function chext {
  for file in *.$1; do
    mv "$file" "${file%.$1}.$2";
  done
}

# Add a suffix to files with given extension
# Usage: addsuffix jpg _foo
function addsuffix {
  for file in *.$1; do
    mv "$file" "${file%.$1}$2.$1";
  done
}

# Highlight a search string in tail output
# Usage: tgrep file.txt foo
function tgrep {
  local color_hlt=`echo -e '\033[47m\033[30m'`
  local color_rst=`echo -e '\033[0m'`
  tail -f "$1" | sed -e "s/$2/$color_hlt & $color_rst/g";
}

# Recursively delete .DS_Store and ._* files
# Usage: cleandot
function cleandot {
  find . -type f -name ".DS_Store" -ls -delete
  find . -type f -name "._*" -ls -delete
}

# List files in common archives
# Usage: lsz file.tar
function lsz {
  if [ $# -ne 1 ]; then
    echo "lsz filename.[tar,tgz,gz,zip,etc]"
    return 1
  fi

  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2|*.tar.gz|*.tar|*.tbz2|*.tgz) tar tvf $1;;
      *.zip) unzip -l $1;;
      *) echo "'$1' unrecognized." ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# Extract common archives
# Usage: extract file.tar
function extract {
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1     ;;
      *.tar.gz)    tar xzf $1     ;;
      *.bz2)       bunzip2 $1     ;;
      *.rar)       unrar e $1     ;;
      *.gz)        gunzip $1      ;;
      *.tar)       tar xf $1      ;;
      *.tbz2)      tar xjf $1     ;;
      *.tgz)       tar xzf $1     ;;
      *.zip)       unzip $1       ;;
      *.Z)         uncompress $1  ;;
      *.7z)        7z x $1        ;;
      *)           echo "'$1' cannot be extracted via extract()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

#### Docs ####

# Colored man pages
# Usage: man ls
function man {
  env LESS_TERMCAP_mb=$(printf "\e[1;31m") \
  LESS_TERMCAP_md=$(printf "\e[1;36m") \
  LESS_TERMCAP_me=$(printf "\e[0m") \
  LESS_TERMCAP_se=$(printf "\e[0m") \
  LESS_TERMCAP_so=$(printf "\e[1;44;33m") \
  LESS_TERMCAP_ue=$(printf "\e[0m") \
  LESS_TERMCAP_us=$(printf "\e[1;32m") \
  man "$@"
}

# Search man pages
# Usage: mangrep ls size
function mangrep {
  man $1 | grep -iC2 --color=always $2 | less
}

#### Web ####

# List a web page external links
# Usage: externallinks google.com
# function externallinks {
#   echo
#   echo "Looking for external links in page $1"
#   local results=`curl -s "$1" | egrep -o '(http|https)://[a-z0-9]*\.[a-z0-9]*\.[a-z0-9]*' | sort | uniq`
#   local total=`echo "$results" | wc -l`
#   echo "$total links found" | xargs
#   echo
#   echo "$results"
# }

#### Admin ####

# Show users that can login
# Usage: loginusers
# function loginusers {
#   echo -e "\n**** Users that can login\n"
#   awk -F":" '!/bin\/false/ && !/^#/ { print "[" $3 "] " $1 " " $6 }' /etc/passwd
#   echo -e "\n**** And have home dir\n"
#   awk -F":" '!/bin\/false/ && /\/home/ { print $1 " [" $3 "] " $6 }' /etc/passwd
# }

# Show users that can’t login
# Usage: nologinusers
# function nologinusers {
#   echo -e "\n**** Users that can't login\n"
#   awk -F":" '/\/bin\/false/ { print "[" $3 "] " $1 " " $6 }' /etc/passwd
#   echo ""
# }
