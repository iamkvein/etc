
for file in ~/.etc/interactive.{colors,prompt,aliases,functions}.sh; do
  [ -r "$file" ] && . "$file"
done

[[ $OSTYPE =~ "darwin" ]] \
  && [ -r ~/.etc/interactive.osx.sh ] \
  && . ~/.etc/interactive.osx.sh

[[ $OSTYPE =~ "linux" ]] \
  && [ -r ~/.etc/interactive.linux.sh ] \
  && . ~/.etc/interactive.linux.sh \
  || test 0

