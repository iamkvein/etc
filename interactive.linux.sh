
# Ban IP
function ban {
  if [ "`id -u`" == "0" ] ; then
    iptables -A INPUT -s $1 -j DROP
  else
    sudo iptables -A INPUT -s $1 -j DROP
  fi
}

